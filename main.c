#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "bsp.h"
#include "nrf_soc.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"

#include "app_timer.h"
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_ppi.h"

#include "nrf_pwr_mgmt.h"
#include "ble_app.h"
#include "twi_nrf52.h"
#include "mlx90632.h"

/**@brief Function for doing power management. */
static void idle_state_handle(void)
{
    nrf_pwr_mgmt_run();
}

/**@brief Function for initializing the power management module. */
static void pwr_mgmt_init(void)
{
    ret_code_t err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{
    ret_code_t err_code;

    // Initialize timer module.
    err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}

/**
 * @brief Function for application main entry.
 */

int main(void)
{
    bool erase_bonds;

    pwr_mgmt_init();
    twi_master_init();
    mlx90632_init();

    timers_init();
    buttons_leds_init(&erase_bonds);
    ble_stack_init();
    gap_params_init();
    gatt_init();
    advertising_init();
    services_init();
    conn_params_init();
    peer_manager_init();

    advertising_start(erase_bonds);

	while(1)
	{
		idle_state_handle();
	}
}
